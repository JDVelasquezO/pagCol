import { IndexComponent } from './Components/IndexComponent.js';
import { ContactComponent } from './Components/ContactComponent.js';
import { FooterComponent } from "./Components/FooterComponent.js";


const Index = { template: '<index-content></index-content>' }
const Contact = { template: '<contact-content></contact-content>' }

const routes = [
  { path: '/', component: Index },
  { path: '/contact', component: Contact }
]

const router = new VueRouter({
  routes
})

export { router }